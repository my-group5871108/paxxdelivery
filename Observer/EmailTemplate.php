<?php

namespace Swift\PaxxDelivery\Observer;

use Swift\PaxxDelivery\Model\Carrier\Carrier;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class EmailTemplate implements ObserverInterface
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function execute(Observer $observer)
    {
        $transport = $observer->getTransport();

        $orderId = $transport->getData(OrderItemInterface::ORDER_ID);
        $order = $this->orderRepository->get($orderId);

        $shippingId = $order->getData(Carrier::SHIPPING_ID);

        if ($shippingId) {
            $transport[Carrier::SHIPPING_ID] = $shippingId;
        }
    }
}
