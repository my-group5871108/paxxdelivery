<?php

namespace Swift\PaxxDelivery\Plugin;

use Swift\PaxxDelivery\Model\Carrier\Carrier;
use Magento\Checkout\Api\ShippingInformationManagementInterface;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Quote\Api\CartRepositoryInterface;

class QuoteManagement
{
    protected $quoteRepository;

    public function __construct(CartRepositoryInterface $quoteRepository)
    {
        $this->quoteRepository = $quoteRepository;
    }

    public function beforeSaveAddressInformation(
        ShippingInformationManagementInterface $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ): void {
        $shippingId = $addressInformation->getExtensionAttributes()->getPaxxShippingId();

        $quote = $this->quoteRepository->get($cartId);
        $quote->setData(Carrier::SHIPPING_ID, $shippingId);
        $this->quoteRepository->save($quote);
    }
}
