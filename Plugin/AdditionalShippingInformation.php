<?php

namespace Swift\PaxxDelivery\Plugin;

use Swift\PaxxDelivery\Model\Carrier\Carrier;

class AdditionalShippingInformation
{
    public function afterEstimateByExtendedAddress($result, $output): array
    {
        foreach ($output as $carrier) {
            $carrierCode = $carrier->getCarrierCode();

            if ($carrierCode === Carrier::CODE) {
                return [$carrier];
            }
        }

        return $output;
    }
}
