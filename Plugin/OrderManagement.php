<?php

namespace Swift\PaxxDelivery\Plugin;

use Swift\PaxxDelivery\Model\Carrier\Carrier;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Quote\Api\CartRepositoryInterface;

class OrderManagement
{
    protected $orderRepository;
    protected $quoteRepository;

    public function __construct(CartRepositoryInterface $quoteRepository, OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->quoteRepository = $quoteRepository;
    }

    public function beforePlace(OrderManagementInterface $subject, OrderInterface $order): void
    {
        $quoteId = $order->getQuoteId();

        if ($quoteId) {
            $quote = $this->quoteRepository->get($quoteId);

            $shippingId = $quote->getData(Carrier::SHIPPING_ID);
            $order->setData(Carrier::SHIPPING_ID, $shippingId);

            $this->orderRepository->save($order);
        }
    }
}
