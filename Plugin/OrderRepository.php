<?php

namespace Swift\PaxxDelivery\Plugin;

use Swift\PaxxDelivery\Model\Carrier\Carrier;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Model\OrderRepository as ModelOrderRepository;

class OrderRepository
{
    public function afterGet(
        ModelOrderRepository $orderRepository,
        OrderInterface $order
    ): OrderInterface {
        $this->setShippingId($order);
        return $order;
    }

    public function afterGetList(
        ModelOrderRepository $orderRepository,
        OrderSearchResultInterface $searchResult
    ): OrderSearchResultInterface {
        foreach ($searchResult->getItems() as $order) {
            $this->setShippingId($order);
        }

        return $searchResult;
    }

    private function setShippingId(OrderInterface $order)
    {
        $shippingId = $order->getData(Carrier::SHIPPING_ID);
        $extensionAttributes = $order->getExtensionAttributes();
        $extensionAttributes->setShippingId($shippingId);
        $order->setExtensionAttributes($extensionAttributes);
    }
}
