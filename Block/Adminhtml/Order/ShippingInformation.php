<?php

namespace Swift\PaxxDelivery\Block\Adminhtml\Order;

use Swift\PaxxDelivery\Model\Carrier\Carrier;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class ShippingInformation extends Template
{
    protected $orderRepository;

    public function __construct(
        Template\Context $context,
        OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->orderRepository = $orderRepository;
    }

    public function getShippingId(): ?string
    {
        $orderId = $this->getOrderId();

        $order = $this->orderRepository->get($orderId);
        $shippingId = $order->getData(Carrier::SHIPPING_ID);

        return $shippingId;
    }

    public function getOrderId(): int
    {
        return $this->getRequest()->getParam(OrderItemInterface::ORDER_ID);
    }
}
